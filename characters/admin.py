# characters/admin.py
from django.contrib import admin
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe

from characters.models import Character


@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    date_hierarchy = "created_at"
    list_display = (
        "id",
        "display_name",
        "_clan_name",
        "created_at",
    )
    list_display_links = (
        "id",
        "display_name",
    )

    def _clan_name(self, character):
        return character.clan

    _clan_name.short_description = _("Clan")
