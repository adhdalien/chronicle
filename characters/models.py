# characters/models.py
from enum import Enum
from django.db import models

from clans.models import Clan


class Character(models.Model):
    id = models.BigAutoField(primary_key=True)
    display_name = models.CharField(max_length=12, unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    clan = models.ForeignKey(
        Clan, on_delete=models.SET_NULL, related_name="members", null=True, blank=True
    )
    clan_rank = models.CharField(max_length=24, null=True, blank=True)

    class Meta:
        db_table = "characters"

    def __str__(self) -> str:
        return f"{self.display_name}"


class CharacterGains(models.Model):
    id = models.BigAutoField(primary_key=True)
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name="gains"
    )

    data = models.JSONField()

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "character_gains"
