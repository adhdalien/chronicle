from __future__ import absolute_import
from typing import List
from celery import shared_task

from characters.models import Character, CharacterGains
from juna.hiscores import hiscores_load

@shared_task(bind=True, track_started=True)
def c_update_character_chains(self, character_ids: List[int]):
    """
    Fetches the latest stats for given character ids and inserts the latest deltas
    into the database as CharacterGains.

    1. Get all the characters in the database with their stats info summed
       up so we can calculate deltas with the new information
    2. Fetch the latest stats information for each character with the juna
       package's hiscores_load function
    3. Calculate the difference to the previous state, and store the deltas
       for each character as CharacterGains
    
    NOTE: What to do when a character disappears/has no new stats? Stop updating?
    """
    return f"Updated gains for {0} characters."
