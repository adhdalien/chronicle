# Chronicle: runescape stats and activities tracking

## Stack

- Django
- Celery/RabbitMQ/Redis for distributed task processing

## Features Overview

- Runescape stats tracking
- Clan tracking (character clan membership can be confirmed via the runescape clan members list api)
- Discord bot with access to the gains information
- Timeseries forecasting to estimate future gains based on current data

## References

- Runescape API: https://runescape.wiki/w/Application_programming_interface
- Runetracker: http://runetracker.org/
- Crystal Math Labs: https://www.crystalmathlabs.com/
- RuneClan: https://www.runeclan.com/

## TODO

1. Find out how hard you can bomb the runescape hiscores API
2. What should we do when a character's name changes?

## Tracker Process

1. Fetch a list of all the characters in the database
2. Split the characters list into rational chunks to speed up parallel processing
3. For each character in the database
    1. Sum up the gains up to this moment
    2. Fetch the latest stats
    3. Calculate difference
    4. Store new gains deltas for that character
