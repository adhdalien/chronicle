import requests
from urllib.parse import urlencode
from typing import Optional, Dict

from juna.types import (
    Game,
    GameMode,
    Skill,
    SkillInfo,
    Activity,
    ActivityInfo,
    StatInfo,
)


def hiscores_player_url(name: str, game: Game, game_mode: GameMode) -> Optional[str]:
    url = None

    if game is Game.RUNESCAPE or game is Game.OLDSCHOOL_RUNESCAPE:
        url = "https://secure.runescape.com/m=hiscore"

        if game is Game.OLDSCHOOL_RUNESCAPE:
            url += "_oldschool"

        if game_mode is GameMode.IRONMAN:
            url += "_ironman"
        elif game_mode is GameMode.HARDCORE_IRONMAN:
            url += "_hardcore_ironman"
        elif game_mode is GameMode.ULTIMATE_IRONMAN:
            url += "_ultimate_ironman"

        params = urlencode({"player": name})
        url += f"/index_lite.ws?{params}"

    return url


def hiscores_parse_line(line: str) -> Optional[StatInfo]:
    parts = line.split(",")
    if len(parts) == 3:
        rank, level, experience = [int(p) for p in parts]
        return SkillInfo({"rank": rank, "level": level, "experience": experience})
    elif len(parts) == 2:
        rank, score = [int(p) for p in parts]
        return ActivityInfo({"rank": rank, "score": score})


def hiscores_load(
    name: str, game: Game, game_mode: GameMode
) -> Optional[Dict[Skill, StatInfo]]:
    hiscores_url = hiscores_player_url(name, game, game_mode)
    hiscores_req = requests.get(hiscores_url)

    # TODO: Better error handling / checking
    if hiscores_req.status_code != 200:
        return None

    hiscores_lines = hiscores_req.text.splitlines()
    stats = [hiscores_parse_line(line) for line in hiscores_lines]

    skills = Skill if game == Game.RUNESCAPE else Skill.oldschool_skills
    activities = Activity if game == Game.RUNESCAPE else Activity.oldschool_activities

    skills_len = len(skills)
    skills = dict(zip(skills, stats[:skills_len]))
    activities = dict(zip(activities, stats[skills_len:]))
    stats = {**skills, **activities}  # TODO: Use | operator on Python 3.9+

    return stats
