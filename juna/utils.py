import requests

# TODO: Do this without django dependency
from django.utils.http import urlencode

from juna.constants import Game, GameMode


def get_hiscores_url(display_name, game, game_mode):
    if game == Game.RUNESCAPE.value or game == Game.OLDSCHOOL_RUNESCAPE.value:
        base_url = "https://secure.runescape.com/m=hiscore"
        if game == Game.OLDSCHOOL_RUNESCAPE:
            base_url += "_oldschool"
        if game_mode == GameMode.IRONMAN:
            base_url += "_ironman"
        elif game_mode == GameMode.HARDCORE_IRONMAN:
            base_url += "_hardcore_ironman"
        elif game_mode == GameMode.ULTIMATE_IRONMAN:
            base_url += "_ultimate_ironman"
        urlparams = urlencode({"player": display_name})
        base_url += f"/index_lite.ws?{urlparams}"
        return base_url
    else:
        return None


def update_gains(self):
    url = self.get_hiscores_url(self.game, self.game_mode)
    print(url)
    req = requests.get(url)

    if req.status_code == 200:
        lines = []
        for line in req.text.splitlines():
            lines.append(self.process_runescape_hiscores_line(line))

        skills_count = len(Skill)
        skills = dict(zip(Skill, lines[:skills_count]))
        activities = dict(zip(Activity, lines[skills_count:]))
        print(skills)
        print(activities)
        gains_data = dict(skills, **activities)

        self.gains.create(data=gains_data)
