import requests
import unicodedata
from urllib.parse import urlencode
from typing import List, Optional


def get_clan_members_list(clan_name: str) -> Optional[List[str]]:
    params = urlencode({"clanName": clan_name})
    list_url = f"http://services.runescape.com/m=clan-hiscores/members_lite.ws?{params}"
    res = unicodedata.normalize("NFKC", requests.get(list_url).text)
    lines = res.splitlines()[1:]
    return [line.split(",")[0] for line in lines]
