from unittest import skip
from django.test import TestCase, testcases

from juna.hiscores import hiscores_player_url, hiscores_parse_line, hiscores_load
from juna.types import Game, GameMode


class HiscoresTestCase(TestCase):
    def test_hiscores_url(self):
        name = "vihellys"
        url_prefix = "https://secure.runescape.com/m="
        url_suffix = f"/index_lite.ws?player={name}"
        expected = (
            (
                name,
                Game.RUNESCAPE,
                GameMode.REGULAR,
                "hiscore",
            ),
            (
                name,
                Game.OLDSCHOOL_RUNESCAPE,
                GameMode.REGULAR,
                "hiscore_oldschool",
            ),
            (name, Game.RUNESCAPE, GameMode.IRONMAN, "hiscore_ironman"),
            (
                name,
                Game.RUNESCAPE,
                GameMode.HARDCORE_IRONMAN,
                "hiscore_hardcore_ironman",
            ),
            (
                name,
                Game.RUNESCAPE,
                GameMode.ULTIMATE_IRONMAN,
                "hiscore_ultimate_ironman",
            ),
        )
        for info in expected:
            name, game, mode, module = info
            expected_url = f"{url_prefix}{module}{url_suffix}"
            assert hiscores_player_url(name, game, mode) == expected_url

    def test_hiscores_parse_lines(self):
        skill_info = hiscores_parse_line("-1,-1,-1")
        assert skill_info["rank"] == -1
        assert skill_info["level"] == -1
        assert skill_info["experience"] == -1
        activity_info = hiscores_parse_line("-1,-1")
        assert activity_info["rank"] == -1
        assert activity_info["score"] == -1

    @skip("Network requests are slow")
    def test_hiscores_load(self):
        name = "vihellys"
        game = Game.RUNESCAPE
        mode = GameMode.REGULAR
        stats = hiscores_load(name, game, mode)


class ClansTestCase(TestCase):
    def test_members_list_load(self):
        from .clans import load_clan_members_list

        tag = "Ataraxia"
        members = load_clan_members_list(tag)
        self.assertIn("Vihellys", members)