import os

from celery import Celery

# set the default django settings module for the "celery" program
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

# create and configure
app = Celery("config", backend="redis", broker="redis://localhost:6379")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
