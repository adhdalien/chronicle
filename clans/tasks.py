# clans/tasks.py
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger

from characters.models import Character
from clans.models import Clan
from juna.clans import get_clan_members_list

logger = get_task_logger(__name__)


@shared_task(bind=True, track_started=True)
def c_update_clan_members(self, clan_name: str):
    """
    Cleans outdated clan members from the database and adds all new ones
    that aren't yet in the database.

    1. Get the latest members list from the RS API
    2. Set clan relation field to NULL on characters that aren't on the list
    3. Create characters that aren't yet in the database
    4. Update existing characters with the proper clan name if they are on
       the list but don't have one set.
    """
    members = get_clan_members_list(clan_name)

    return f"Cleaned up {0} old members and added {0} new members for {0}"
