# clans/admin.py
from django.contrib import admin

from clans.models import Clan


@admin.register(Clan)
class ClanAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "_members",
    )

    def _members(self, obj):
        return obj.members.all().count()
