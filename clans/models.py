# clans/models.py
from django.db import models


class Clan(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=25, unique=True, null=False, blank=False)

    class Meta:
        db_table = "clans"

    def __str__(self) -> str:
        return f"{self.name}"
